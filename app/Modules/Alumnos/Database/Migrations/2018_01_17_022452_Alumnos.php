<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alumnos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nivel');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('representantes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('dni');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('profesores_especiales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('dni');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('profesores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('dni');
            $table->integer('grado_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('grado_id')
                  ->references('id')->on('grados')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::create('alumnos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('representante_id')->unsigned();
            $table->integer('grado_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('representante_id')
                  ->references('id')->on('representantes')
                  ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('grado_id')
                ->references('id')->on('grados')
                ->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::create('asignaturas', function(Blueprint $table){
            $table->increments('id');
            $table->string('nombre');
            $table->string('codigo');
            $table->integer('grado_id')->unsigned();
            $table->integer('profesor_especial_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('grado_id')
                ->references('id')->on('grados')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('profesor_especial_id')
                ->references('id')->on('profesores_especiales')
                ->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::create('boletin', function(Blueprint $table){
            $table->increments('id');
            $table->integer('grado_id')->nulleable()->unsigned();
            $table->integer('asignatura_id')->unsigned();
            $table->string('notas');
            $table->integer('alumno_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('grado_id')
                ->references('id')->on('grados')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('asignatura_id')
                ->references('id')->on('asignaturas')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('alumno_id')
                ->references('id')->on('alumnos')
                ->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletin');
        Schema::dropIfExists('asignaturas');
        Schema::dropIfExists('alumnos');
        Schema::dropIfExists('profesores');
        Schema::dropIfExists('profesores_especiales');
        Schema::dropIfExists('representantes');
        Schema::dropIfExists('grados');
    }
}
