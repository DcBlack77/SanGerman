<?php

$menu['alumnos'] = [
    [
        'nombre'        => 'Colegio',
        'dirección'     => '#Colegio',
        'icono'         => 'fa fa-note',
        'menu'          => [
            [
				'nombre' 	=> 'Grados',
				'direccion' => 'alumnos/grados',
				'icono' 	=> 'fa fa-user'
			],
			[
				'nombre' 	=> 'Representantes',
				'direccion' => 'alumnos/representantes',
				'icono' 	=> 'fa fa-users'
			],
			[
				'nombre' 	=> 'Profesores Especiales',
				'direccion' => 'alumnos/profesores_especiales',
				'icono' 	=> 'fa fa-users'
			],
			[
				'nombre' 	=> 'Profesores',
				'direccion' => 'alumnos/profesores',
				'icono' 	=> 'fa fa-users'
			],
			[
				'nombre' 	=> 'Alumnos',
				'direccion' => 'alumnos/alumnos',
				'icono' 	=> 'fa fa-users'
			],
			[
				'nombre' 	=> 'Asignaturas',
				'direccion' => 'alumnos/asignaturas',
				'icono' 	=> 'fa fa-users'
			],
			[
				'nombre' 	=> 'Boletín',
				'direccion' => 'alumnos/boletin',
				'icono' 	=> 'fa fa-users'
			],
        ]
    ]
]

 ?>
