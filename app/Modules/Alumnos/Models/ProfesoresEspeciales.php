<?php

namespace App\Modules\Alumnos\Models;

use App\Modules\Base\Models\Modelo;



class ProfesoresEspeciales extends Modelo
{
    protected $table = 'profesores_especiales';
    protected $fillable = ["nombre","dni"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Profesores Especiales'
    ],
    'dni' => [
        'type' => 'text',
        'label' => 'Dni',
        'placeholder' => 'Dni del Profesores Especiales'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}