<?php

namespace App\Modules\Alumnos\Models;

use App\Modules\Base\Models\Modelo;



class Grados extends Modelo
{
    protected $table = 'grados';
    protected $fillable = ["nivel"];
    protected $campos = [
    'nivel' => [
        'type' => 'text',
        'label' => 'Nivel',
        'placeholder' => 'Nivel del Grados'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}