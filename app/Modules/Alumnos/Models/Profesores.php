<?php

namespace App\Modules\Alumnos\Models;

use App\Modules\Base\Models\Modelo;

use App\Modules\Alumnos\Models\Grados;



class Profesores extends Modelo
{
    protected $table = 'profesores';
    protected $fillable = ["nombre","dni","grado_id"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Profesores'
    ],
    'dni' => [
        'type' => 'text',
        'label' => 'Dni',
        'placeholder' => 'Dni del Profesores'
    ],
    'grado_id' => [
        'type' => 'select',
        'label' => 'Grado',
        'placeholder' => '- Seleccione un Grado',
        'url' => 'Agrega una URL Aqui!'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['grado_id']['options'] = Grados::pluck('nivel', 'id');

    }


}
