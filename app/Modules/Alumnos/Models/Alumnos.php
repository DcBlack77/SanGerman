<?php

namespace App\Modules\Alumnos\Models;

use App\Modules\Base\Models\Modelo;

use App\Modules\Alumnos\Models\Grados;
use App\Modules\Alumnos\Models\Representantes;

class Alumnos extends Modelo
{
    protected $table = 'alumnos';
    protected $fillable = ["nombre","representante_id","grado_id"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Alumnos'
    ],
    'representante_id' => [
        'type' => 'select',
        'label' => 'Representante',
        'placeholder' => '- Seleccione un Representante',
        'url' => 'Agrega una URL Aqui!'
    ],
    'grado_id' => [
        'type' => 'select',
        'label' => 'Grado',
        'placeholder' => '- Seleccione un Grado',
        'url' => 'Agrega una URL Aqui!'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['grado_id']['options'] = Grados::pluck('nivel', 'id');
        $this->campos['representante_id']['options'] = Representantes::pluck('nombre', 'id');

    }


}
