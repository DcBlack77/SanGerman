<?php

namespace App\Modules\Alumnos\Http\Requests;

use App\Http\Requests\Request;

class BoletinRequest extends Request {
    protected $reglasArr = [
		'grado_id' => ['integer'],
		'asignatura_id' => ['required', 'integer'],
		'notas' => ['required'],
		'alumno_id' => ['required', 'integer']
	];
}
