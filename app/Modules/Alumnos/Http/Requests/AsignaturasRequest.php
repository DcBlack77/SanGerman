<?php

namespace App\Modules\Alumnos\Http\Requests;

use App\Http\Requests\Request;

class AsignaturasRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:255'], 
		'codigo' => ['required', 'min:3', 'max:255'], 
		'grado_id' => ['required', 'integer'], 
		'profesor_especial_id' => ['integer']
	];
}