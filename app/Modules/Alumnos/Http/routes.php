<?php

Route::group(['middleware' => 'web', 'prefix' => 'alumnos', 'namespace' => 'App\\Modules\Alumnos\Http\Controllers'], function()
{
    Route::get('/', 'AlumnosController@index');


        Route::group(['prefix' => 'grados'], function() {
            Route::get('/',                 'GradosController@index');
            Route::get('nuevo',             'GradosController@nuevo');
            Route::get('cambiar/{id}',      'GradosController@cambiar');

            Route::get('buscar/{id}',       'GradosController@buscar');

            Route::post('guardar',          'GradosController@guardar');
            Route::put('guardar/{id}',      'GradosController@guardar');

            Route::delete('eliminar/{id}',  'GradosController@eliminar');
            Route::post('restaurar/{id}',   'GradosController@restaurar');
            Route::delete('destruir/{id}',  'GradosController@destruir');

            Route::get('datatable',         'GradosController@datatable');
        });

        Route::group(['prefix' => 'alumnos'], function() {
            Route::get('/',                 'AlumnosController@index');
            Route::get('nuevo',             'AlumnosController@nuevo');
            Route::get('cambiar/{id}',      'AlumnosController@cambiar');

            Route::get('buscar/{id}',       'AlumnosController@buscar');

            Route::post('guardar',          'AlumnosController@guardar');
            Route::put('guardar/{id}',      'AlumnosController@guardar');

            Route::delete('eliminar/{id}',  'AlumnosController@eliminar');
            Route::post('restaurar/{id}',   'AlumnosController@restaurar');
            Route::delete('destruir/{id}',  'AlumnosController@destruir');

            Route::get('datatable',         'AlumnosController@datatable');
        });


        Route::group(['prefix' => 'representantes'], function() {
            Route::get('/',                 'RepresentantesController@index');
            Route::get('nuevo',             'RepresentantesController@nuevo');
            Route::get('cambiar/{id}',      'RepresentantesController@cambiar');

            Route::get('buscar/{id}',       'RepresentantesController@buscar');

            Route::post('guardar',          'RepresentantesController@guardar');
            Route::put('guardar/{id}',      'RepresentantesController@guardar');

            Route::delete('eliminar/{id}',  'RepresentantesController@eliminar');
            Route::post('restaurar/{id}',   'RepresentantesController@restaurar');
            Route::delete('destruir/{id}',  'RepresentantesController@destruir');

            Route::get('datatable',         'RepresentantesController@datatable');
        });


        Route::group(['prefix' => 'profesores_especiales'], function() {
            Route::get('/',                 'ProfesoresEspecialesController@index');
            Route::get('nuevo',             'ProfesoresEspecialesController@nuevo');
            Route::get('cambiar/{id}',      'ProfesoresEspecialesController@cambiar');

            Route::get('buscar/{id}',       'ProfesoresEspecialesController@buscar');

            Route::post('guardar',          'ProfesoresEspecialesController@guardar');
            Route::put('guardar/{id}',      'ProfesoresEspecialesController@guardar');

            Route::delete('eliminar/{id}',  'ProfesoresEspecialesController@eliminar');
            Route::post('restaurar/{id}',   'ProfesoresEspecialesController@restaurar');
            Route::delete('destruir/{id}',  'ProfesoresEspecialesController@destruir');

            Route::get('datatable',         'ProfesoresEspecialesController@datatable');
        });


        Route::group(['prefix' => 'profesores'], function() {
            Route::get('/',                 'ProfesoresController@index');
            Route::get('nuevo',             'ProfesoresController@nuevo');
            Route::get('cambiar/{id}',      'ProfesoresController@cambiar');

            Route::get('buscar/{id}',       'ProfesoresController@buscar');

            Route::post('guardar',          'ProfesoresController@guardar');
            Route::put('guardar/{id}',      'ProfesoresController@guardar');

            Route::delete('eliminar/{id}',  'ProfesoresController@eliminar');
            Route::post('restaurar/{id}',   'ProfesoresController@restaurar');
            Route::delete('destruir/{id}',  'ProfesoresController@destruir');

            Route::get('datatable',         'ProfesoresController@datatable');
        });


        Route::group(['prefix' => 'asignaturas'], function() {
            Route::get('/',                 'AsignaturasController@index');
            Route::get('nuevo',             'AsignaturasController@nuevo');
            Route::get('cambiar/{id}',      'AsignaturasController@cambiar');

            Route::get('buscar/{id}',       'AsignaturasController@buscar');

            Route::post('guardar',          'AsignaturasController@guardar');
            Route::put('guardar/{id}',      'AsignaturasController@guardar');

            Route::delete('eliminar/{id}',  'AsignaturasController@eliminar');
            Route::post('restaurar/{id}',   'AsignaturasController@restaurar');
            Route::delete('destruir/{id}',  'AsignaturasController@destruir');

            Route::get('datatable',         'AsignaturasController@datatable');
        });


        Route::group(['prefix' => 'boletin'], function() {
            Route::get('/',                 'BoletinController@index');
            Route::get('nuevo',             'BoletinController@nuevo');
            Route::get('cambiar/{id}',      'BoletinController@cambiar');

            Route::get('buscar/{id}',       'BoletinController@buscar');

            Route::post('guardar',          'BoletinController@guardar');
            Route::put('guardar/{id}',      'BoletinController@guardar');

            Route::delete('eliminar/{id}',  'BoletinController@eliminar');
            Route::post('restaurar/{id}',   'BoletinController@restaurar');
            Route::delete('destruir/{id}',  'BoletinController@destruir');

            Route::get('datatable',         'BoletinController@datatable');
        });

    //{{route}}
});
