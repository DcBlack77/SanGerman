<?php

namespace App\Modules\Alumnos\Http\Controllers;

//Controlador Padre
use App\Modules\Alumnos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Alumnos\Http\Requests\BoletinRequest;

//Modelos
use App\Modules\Alumnos\Models\Boletin;
use App\Modules\Alumnos\Models\Grados;
use App\Modules\Alumnos\Models\Alumnos;
use App\Modules\Alumnos\Models\Representantes;

class BoletinController extends Controller
{
    protected $titulo = 'Boletin';

    public $js = [
        'Boletin'
    ];

    public $css = [
        'Boletin'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('alumnos::Boletin', [
            'Boletin' => new Boletin()
        ]);
    }

    public function nuevo()
    {
        $Boletin = new Boletin();
        return $this->view('alumnos::Boletin', [
            'layouts' => 'base::layouts.popup',
            'Boletin' => $Boletin
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Boletin = Boletin::find($id);
        return $this->view('alumnos::Boletin', [
            'layouts' => 'base::layouts.popup',
            'Boletin' => $Boletin
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Boletin = Boletin::withTrashed()->find($id);
        } else {
            $Boletin = Boletin::find($id);
        }

        if ($Boletin) {
            return array_merge($Boletin->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(BoletinRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Boletin = $id == 0 ? new Boletin() : Boletin::find($id);

            $Boletin->fill($request->all());
            $Boletin->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Boletin->id,
            'texto' => $Boletin->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Boletin::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Boletin::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Boletin::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Boletin::select([
            'id', 'grado_id', 'asignatura_id', 'notas', 'alumno_id', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    // public function grados() {
    //     $grados = Grados::pluck('nivel', 'id');
    //     return $grados;
    // }
    //
    // public function representantes()
    // {
    //     $representantes = Representantes::pluck('nombre', 'id');
    //     return $representantes;
    // }
    //
    // public function alumnos()
    // {
    //     $alumnos = Alumnos::pluck('nombre', 'id');
    //     return $alumnos;
    // }

}
