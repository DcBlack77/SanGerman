<?php

namespace App\Modules\Alumnos\Http\Controllers;

//Controlador Padre
use App\Modules\Alumnos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Alumnos\Http\Requests\ProfesoresEspecialesRequest;

//Modelos
use App\Modules\Alumnos\Models\ProfesoresEspeciales;

class ProfesoresEspecialesController extends Controller
{
    protected $titulo = 'Profesores Especiales';

    public $js = [
        'ProfesoresEspeciales'
    ];
    
    public $css = [
        'ProfesoresEspeciales'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('alumnos::ProfesoresEspeciales', [
            'ProfesoresEspeciales' => new ProfesoresEspeciales()
        ]);
    }

    public function nuevo()
    {
        $ProfesoresEspeciales = new ProfesoresEspeciales();
        return $this->view('alumnos::ProfesoresEspeciales', [
            'layouts' => 'base::layouts.popup',
            'ProfesoresEspeciales' => $ProfesoresEspeciales
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $ProfesoresEspeciales = ProfesoresEspeciales::find($id);
        return $this->view('alumnos::ProfesoresEspeciales', [
            'layouts' => 'base::layouts.popup',
            'ProfesoresEspeciales' => $ProfesoresEspeciales
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $ProfesoresEspeciales = ProfesoresEspeciales::withTrashed()->find($id);
        } else {
            $ProfesoresEspeciales = ProfesoresEspeciales::find($id);
        }

        if ($ProfesoresEspeciales) {
            return array_merge($ProfesoresEspeciales->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ProfesoresEspecialesRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $ProfesoresEspeciales = $id == 0 ? new ProfesoresEspeciales() : ProfesoresEspeciales::find($id);

            $ProfesoresEspeciales->fill($request->all());
            $ProfesoresEspeciales->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $ProfesoresEspeciales->id,
            'texto' => $ProfesoresEspeciales->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            ProfesoresEspeciales::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            ProfesoresEspeciales::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            ProfesoresEspeciales::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = ProfesoresEspeciales::select([
            'id', 'nombre', 'dni', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}