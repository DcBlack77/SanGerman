<?php

namespace App\Modules\Alumnos\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	public $app = 'alumnos';

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'app/Modules/Alumnos/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'app/Modules/Alumnos/Assets/css',
	];
}
