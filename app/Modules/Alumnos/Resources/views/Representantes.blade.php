@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Representantes']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Representantes.',
        'columnas' => [
            'Nombre' => '50',
		'Dni' => '50'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Representantes->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection