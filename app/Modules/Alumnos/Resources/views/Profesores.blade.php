@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Profesores']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Profesores.',
        'columnas' => [
            'Nombre' => '33.333333333333',
		'Dni' => '33.333333333333',
		'Grado' => '33.333333333333'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Profesores->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection