@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')

    @include('base::partials.ubicacion', ['ubicacion' => ['Boletin']])

    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Boletin.',
        'columnas' => [
            'Grado' => '25',
    		'Asignatura' => '25',
    		'Notas' => '25',
    		'Alumno' => '25'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Boletin->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection
