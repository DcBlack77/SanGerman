@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Asignaturas']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Asignaturas.',
        'columnas' => [
            'Nombre' => '25',
		'Codigo' => '25',
		'Grado' => '25',
		'Profesor Especial' => '25'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Asignaturas->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection