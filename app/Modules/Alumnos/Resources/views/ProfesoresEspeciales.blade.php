@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Profesores Especiales']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar ProfesoresEspeciales.',
        'columnas' => [
            'Nombre' => '50',
		'Dni' => '50'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $ProfesoresEspeciales->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection