@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Grados']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Grados.',
        'columnas' => [
            'Nivel' => '100'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Grados->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection